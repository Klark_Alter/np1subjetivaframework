package br.edu.uniateneu.webservice.sub.NP1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebserviceSubNp1Application {

	public static void main(String[] args) {
		SpringApplication.run(WebserviceSubNp1Application.class, args);
	}

}
